
jQuery(document).ready(function() {
	jQuery('#saved_addresses').change(function(){
		var obj;
		var alertTxt = '';
		obj = jQuery.parseJSON(Drupal.settings.commerce_addressbook);
		var selectedVal = jQuery(this).val();
		for(i in obj[selectedVal]){
			if(i !== 'undefined')
				update_address_field(i, obj[selectedVal][i]);
		}
	})	
});

function update_address_field(name,val){
	var ids;
	ids = jQuery.parseJSON(Drupal.settings.addressfield_ids);
	jQuery('#'+ids[name]).val(val);
	
}
